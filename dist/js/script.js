'use strict';

$(document).ready(function () {

  // PopUp
  function popUp() {
    $('.js-popup-button').on('click', function (e) {
      e.preventDefault();

      var popupClass = '.' + $(this).attr('data-popupShow');

      $('.popup').removeClass('js-popup-show');

      $(popupClass).addClass('js-popup-show');
      $('body').addClass('no-scrolling');
    });

    // Close PopUp
    $('.js-close-popup').on('click', function (e) {
      e.preventDefault();

      $('.popup').removeClass('js-popup-show');
      $('body').removeClass('no-scrolling');
    });

    $('.popup').on('click', function (e) {
      var div = $(".popup__wrap");

      if (!div.is(e.target) && div.has(e.target).length === 0) {
        $('.popup').removeClass('js-popup-show');
        $('body').removeClass('no-scrolling');
      }
    });
  }

  popUp();

  // Scroll
  $('#personal-info').find('.table-wrap__inner').scrollbar();
  $('#main').find('.winners-table__wrap').scrollbar();

  // Masked Phone
  $('input[type="tel"]').mask('+7(999)999-99-99');

  $('.card-trans').mask('9999-9999-9999-9999');

  // Tabs
  function openTabs() {
    var wrap = $('#personal-info');
    var tabsContent = wrap.find('.table-wrap');
    var tabsBtn = wrap.find('.tabs li');

    // Reset
    tabsBtn.eq(0).addClass('active');
    tabsContent.eq(0).addClass('active');

    // Tabs Main Action
    wrap.find('.tabs').on('click', 'li', function () {
      var i = $(this).index();

      tabsBtn.removeClass('active');
      $(this).addClass('active');
      tabsContent.removeClass('active');
      tabsContent.eq(i).addClass('active');
    });
  }

  openTabs();

  function formatState(opt) {
    if (!opt.id) {
      return opt.text;
    }

    var optimage = $(opt.element).attr('data-image');

    if (!optimage) {
      return opt.text;
    } else {
      var $opt = $(
      '<span class="select2-image"><div class="select2-image__img"><img src="' + optimage + '"/></div> ' + opt.text + '</span>');

      return $opt;
    }
  }

  $('select').each(function () {
    var placeholder = $(this).attr('data-place');

    $(this).select2({
      placeholder: placeholder,
      dropdownParent: $(this).closest('.form__input') });

  });

  // Select
  $('.image-select select').each(function () {
    $(this).select2({
      templateResult: formatState,
      templateSelection: formatState,
      dropdownParent: $(this).closest('.image-select') });

  });

  $('select').on('select2:open', function () {
    $('.select2-results__options').scrollbar();
  });

  // Jquery Validate
  function checkValidate() {
    var form = $('form');

    $.each(form, function () {
      $(this).validate({
        ignore: [],
        errorClass: 'error',
        validClass: 'success',
        rules: {
          name: {
            required: true,
            letters: true },

          surname: {
            required: true,
            letters: true },

          secondName: {
            required: true,
            letters: true },

          passNum: {
            required: true },

          passDate: {
            required: true },

          passINN: {
            required: true },

          prizeRegion: {
            required: true },

          prizeHouse: {
            required: true },

          theme: {
            required: true },

          sms: {
            required: true },

          prizeStreet: {
            required: true },

          prizeCity: {
            required: true },

          passHead: {
            required: true },

          passAddr: {
            required: true },

          passScanINN: {
            required: true },

          kv: {
            required: true },

          email: {
            required: true,
            email: true },

          phone: {
            required: true,
            phone: true },

          city: {
            required: true,
            letters: true },

          captcha: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          password: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          home: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          apartment: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          textarea: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } } },


        messages: {
          phone: 'Некорректный номер',
          passHead: 'Загрузите файл',
          passAddr: 'Загрузите файл',
          passScanINN: 'Загрузите файл',
          rules: {
            required: '' },

          personal: {
            required: '' } } });




      jQuery.validator.addMethod('letters', function (value, element) {
        return this.optional(element) || /^([a-zа-яё]+)$/i.test(value);
      });

      jQuery.validator.addMethod('email', function (value, element) {
        return this.optional(element) || /\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6}/.test(value);
      });

      jQuery.validator.addMethod('phone', function (value, element) {
        return this.optional(element) || /\+7\(\d+\)\d{3}-\d{2}-\d{2}/.test(value);
      });

    });

  }

  checkValidate();

  // main screen
  function mainHeight() {
    var footerH = $('.footer').outerHeight();
    var headerH = $('.header').outerHeight();
    var winH = $(window).outerHeight();

    $('.main-screen').css('height', "".concat(winH - footerH - headerH, "px"));
  }

  if ($('.main-screen').length != 0) {
    mainHeight();

    $(window).on('resize', function () {return mainHeight();});
  }

  // Cookies ---------
  // Check cookie name
  function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));


    return matches ? decodeURIComponent(matches[1]) : undefined;
  }

  function cookie() {
    var cookieElem = $('.cookie');
    var data = getCookie('isCookie');

    if (data) {
      cookieElem.removeClass('active');
    } else {
      cookieElem.addClass('active');

      $('.cookie .btn').on('click', function (e) {
        e.preventDefault();

        document.cookie = "isCookie=true";
        cookieElem.removeClass('active');
      });
    }
  }

  cookie();

  // Burger
  $('.header__burger').on('click', function () {
    $('.header').toggleClass('active');
  });

  $('body').on('click', function (e) {
    var div = $(".header__mobNav, .header__burger");

    if (!div.is(e.target) && div.has(e.target).length === 0) {
      $('.header').removeClass('active');
    }
  });

  // Personal
  $('.header__personal-area').on('click', function () {
    $('.header').toggleClass('personal-active');
  });

  $('body').on('click', function (e) {
    var div = $(".header__personal-area, .header__mobNav");

    if (!div.is(e.target) && div.has(e.target).length === 0) {
      $('.header').removeClass('personal-active');
    }
  });

  $('.js-show-social').on('click', function () {
    $(this).slideUp(200);
    $('.personal__social').slideDown(300);
  });

  // Kladrapi
  // function kladrInit() {
  //     const popup = $('.get-prize, .get-prize-big');
  //     const $region = popup.find('input[name="prizeRegion"]');
  //     const $city = popup.find('input[name="prizeCity"]');
  //     const $street = popup.find('input[name="prizeStreet"]');
  //     const $building = popup.find('input[name="prizeHouse"]');
  //
  //     $.kladr.setDefault({
  //         parentInput: '.form__input',
  //         verify: true,
  //         withParents: true,
  //         limit: 100,
  //         spinner: false
  //     });
  //
  //     $region.kladr({type: $.kladr.type.region});
  //     $city.kladr({type: $.kladr.type.city});
  //     $street.kladr({type: $.kladr.type.street});
  //     $building.kladr({type: $.kladr.type.building});
  // }
  //
  // kladrInit();
  //
  // $('input[name="prizeRegion"], input[name="prizeCity"], input[name="prizeStreet"], input[name="prizeHouse"]').on('kladr_open', function () {
  //     $(this).addClass('active');
  // });
  //
  // $('input[name="prizeRegion"], input[name="prizeCity"], input[name="prizeStreet"], input[name="prizeHouse"]').on('kladr_close', function () {
  //     $(this).removeClass('active');
  // });

  // Type file
  $('body').on('change', '.form__input-file input[type="file"]', function () {
    if ($(this)[0].files.length != 0) {
      $(this).closest('.form__input-file').addClass('active');
    }
  });

  $('body').on('click', '.form__input-file-close', function () {
    var parent = $(this).closest('.form__input-file');
    var input = parent.find('input');
    var inputName = input.attr('name');

    input.remove();

    parent.find('label').append("<input type=\"file\" name=\"".concat(inputName, "\" id=\"").concat(inputName, "\">"));

    parent.removeClass('active');
  });

  // radio
  $('.transfer-card .radio input[type="radio"]').each(function () {
    var parent = $(this).closest('.radio');
    var valueInput = parent.find('.radio__data input').val();

    if (valueInput != '') {
      parent.addClass('card-found');
    } else {
      if (!$(this).is(':checked')) {
        parent.addClass('card-not-found');
      }
    }
  });

  $('.transfer-card .radio input[type="radio"]').on('change', function () {
    $('.transfer-card .radio input[type="radio"]').each(function () {
      var parent = $(this).closest('.radio');
      var valueInput = parent.find('.radio__data input').val();
      parent.
      removeClass('card-found').
      removeClass('card-not-found');

      if (valueInput != '') {
        parent.addClass('card-found');
      } else {
        if (!$(this).is(':checked')) {
          parent.addClass('card-not-found');
        } else {
          parent.removeClass('card-not-found');
        }
      }
    });
  });

  // Clamping Footer
  function clampingFooter() {
    var footerH = $('.footer').outerHeight();
    var headerH = $('.header').outerHeight();
    var WindowH = $(window).outerHeight();

    $('.main').css('min-height', WindowH - headerH - footerH + 'px');
  }

  if ($('.main-screen').length == 0) {
    clampingFooter();

    $(window).on('resize', function () {
      clampingFooter();
    });
  }

  // Accordion
  function openAccordion() {
    var wrap = $('#faq');
    var accordion = wrap.find('.faq__title');

    accordion.on('click', function () {
      var $this = $(this);
      var content = $this.next();
      if (content.hasClass('faq__text_active'))
      $('.faq__text_active').removeClass('faq__text_active');else
      {
        $('.faq__text_active').removeClass('faq__text_active');
        content.addClass('faq__text_active');
      }
    });
  }
  openAccordion();
});